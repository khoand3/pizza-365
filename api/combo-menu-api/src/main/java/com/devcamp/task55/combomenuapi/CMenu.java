package com.devcamp.task55.combomenuapi;

public class CMenu {
   private String size;
   private String duongKinh;
   private String suonNuong;
   private String salad;
   private String nuocNgot;

   public CMenu(String size, String duongKinh, String suonNuong, String salad, String nuocNgot, String gia) {
      this.size = size;
      this.duongKinh = duongKinh;
      this.suonNuong = suonNuong;
      this.salad = salad;
      this.nuocNgot = nuocNgot;
      this.gia = gia;
   }

   private String gia;

   public String getSize() {
      return size;
   }

   public void setSize(String size) {
      this.size = size;
   }

   public String getDuongKinh() {
      return duongKinh;
   }

   public void setDuongKinh(String duongKinh) {
      this.duongKinh = duongKinh;
   }

   public String getSuonNuong() {
      return suonNuong;
   }

   public void setSuonNuong(String suonNuong) {
      this.suonNuong = suonNuong;
   }

   public String getSalad() {
      return salad;
   }

   public void setSalad(String salad) {
      this.salad = salad;
   }

   public String getNuocNgot() {
      return nuocNgot;
   }

   public void setNuocNgot(String nuocNgot) {
      this.nuocNgot = nuocNgot;
   }

   public String getGia() {
      return gia;
   }

   public void setGia(String gia) {
      this.gia = gia;
   }
}
