package com.devcamp.task55.combomenuapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CComboMenu {
   @GetMapping("")
   public ArrayList<CMenu> getComboMenu() {
      ArrayList<CMenu> menuList = new ArrayList<>();
      menuList.add(new CMenu("S", "20", "2", "200", "2", "150000"));
      menuList.add(new CMenu("M", "25", "4", "300", "3", "200000"));
      menuList.add(new CMenu("L", "30", "8", "500", "4", "250000"));
      return menuList;
   }
}
