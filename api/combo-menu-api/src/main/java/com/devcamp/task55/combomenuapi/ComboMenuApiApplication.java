package com.devcamp.task55.combomenuapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComboMenuApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComboMenuApiApplication.class, args);
	}

}
