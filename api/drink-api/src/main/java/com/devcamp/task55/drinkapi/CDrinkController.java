package com.devcamp.task55.drinkapi;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CDrinkController {
   @GetMapping("")
   public ArrayList<CDrink> getDrinkList() {
      ArrayList<CDrink> drinkList = new ArrayList<>();
      drinkList.add(new CDrink("TRATAC", "Trà tắc", 10000, null, new Date(), new Date()));
      drinkList.add(new CDrink("COCA", "Cocacola", 15000, null, new Date(), new Date()));
      drinkList.add(new CDrink("PEPSI", "Pepsi", 15000, null, new Date(), new Date()));
      drinkList.add(new CDrink("LAVIE", "Lavie", 5000, null, new Date(), new Date()));
      drinkList.add(new CDrink("TRASUA", "Trà sữa trân châu", 40000, null, new Date(), new Date()));
      drinkList.add(new CDrink("FANTA", "Fanta", 15000, null, new Date(), new Date()));
      return drinkList;
   }
}
