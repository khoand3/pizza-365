package com.devcamp.task55.dailycampaignapi;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
public class CDailyCampaign {
   @GetMapping("/daily-campaign")
   public String getDailyCampaign() {
      DateTimeFormatter dtfVietnam = DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
      LocalDate today = LocalDate.now(ZoneId.systemDefault());
      String dayOfWeek = String.format("%s", dtfVietnam.format(today));
      String message = "";
      switch (dayOfWeek) {
         case "Thứ Hai":
            message = dayOfWeek + ": Mua 1 tặng 1.";
            break;
         case "Thứ Ba":
            message = dayOfWeek + ": Tặng tất cả khách hàng một phần bánh ngọt.";
            break;
         case "Thứ Tư":
            message = dayOfWeek + ": Tặng tất cả khách hàng một phần nước ngọt.";
            break;
         case "Thứ Năm":
            message = dayOfWeek + ": Tặng tất cả khách hàng một phần salad.";
            break;
         case "Thứ Sáu":
            message = dayOfWeek + ": Tặng tất cả khách hàng một phần sườn nướng.";
            break;
         case "Thứ Bảy":
            message = dayOfWeek + ": Mua 1 combo size L tặng 1 combo size S.";
            break;
         case "Chủ Nhật":
            message = dayOfWeek + ": Upsize combo đồng giá.";
            break;
      }
      return message;
   }
}
