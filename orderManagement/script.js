'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//khai bao bien toan cuc luu gia tri id cua don hang duoc chon
var gId;
//khai bao bien toan cuc luu gia tri order id cua don hang duoc chon
var gOrderId;
//khai bao bien luu tru kich co combo
const gCOMBO_SIZE = ['S', 'M', 'L'];
//khai bao bien luu tru trang thai order
const gORDER_STATUS = ['Open', 'Confirmed', 'Cancel'];
//khai bao bien luu tru loai pizza
const gPIZZA_TYPE = ['Hawaii', 'Bacon', 'Seafood'];
const gDATA_COLS = [
  'orderId',
  'kichCo',
  'loaiPizza',
  'idLoaiNuocUong',
  'thanhTien',
  'hoTen',
  'soDienThoai',
  'trangThai',
  'chiTiet',
];
const gCOLUMN_ORDER_ID = 0;
const gCOLUMN_KICH_CO = 1;
const gCOLUMN_LOAI_PIZZA = 2;
const gCOLUMN_NUOC_UONG = 3;
const gCOLUMN_THANH_TIEN = 4;
const gCOLUMN_HO_TEN = 5;
const gCOLUMN_SO_DIEN_THOAI = 6;
const gCOLUMN_TRANG_THAI = 7;
const gCOLUMN_CHI_TIET = 8;

// định nghĩa table  - chưa có data
var gOrderTable = $('#orders-table').DataTable({
  // Khai báo các cột của datatable
  columns: [
    { data: gDATA_COLS[gCOLUMN_ORDER_ID] },
    { data: gDATA_COLS[gCOLUMN_KICH_CO] },
    { data: gDATA_COLS[gCOLUMN_LOAI_PIZZA] },
    { data: gDATA_COLS[gCOLUMN_NUOC_UONG] },
    { data: gDATA_COLS[gCOLUMN_THANH_TIEN] },
    { data: gDATA_COLS[gCOLUMN_HO_TEN] },
    { data: gDATA_COLS[gCOLUMN_SO_DIEN_THOAI] },
    { data: gDATA_COLS[gCOLUMN_TRANG_THAI] },
    { data: gDATA_COLS[gCOLUMN_CHI_TIET] },
  ],
  // Ghi đè nội dung của cột chi tiet, chuyển thành button chi tiết
  columnDefs: [
    {
      targets: gCOLUMN_CHI_TIET,
      defaultContent:
        "<button class='order-info btn btn-info'>Chi tiết</button>",
    },
  ],
});
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly khi load trang
  onPageLoading();
  //gan su kien click cho nut chi tiet
  $('#orders-table').on('click', '.order-info', function () {
    onBtnDetailClick(this);
  });
  //gan su kien click cho nut loc
  $('#btn-filter-order').on('click', function () {
    onBtnFilterOrderClick();
  });
  //gan su kien click cho nut confirm tren modal
  $('#btn-confirm').on('click', function () {
    onBtnModalConfirmClick();
  });
  //gan su kien click cho nut cancel tren modal
  $('#btn-cancel').on('click', function () {
    onBtnModalCancelClick();
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly khi load trang
function onPageLoading() {
  'use strict';
  //goi ham goi api de lay du lieu don hang
  callApiToGetOrders();
  //goi ham load danh sach don hang len data table
  loadDataToDataTable();
  //goi ham load cac option vao select trang thai order
  loadOptionToSelectOrderStatus();
  //goi ham load cac option vao select loai pizza
  loadOptionToSelectPizzaType();
}
//ham xu ly su kien khi an nut chi tiet
function onBtnDetailClick(paramElement) {
  'use strict';
  //1. thu thap du lieu
  var vCurrentRow = $(paramElement).closest('tr');
  var vCurrentRowData = gOrderTable.row(vCurrentRow).data();
  gId = vCurrentRowData.id;
  gOrderId = vCurrentRowData.orderId;
  //2. kiem tra du lieu(ko co)
  //3. xu ly hien thi
  //goi ham goi api de lay du lieu order theo order id
  callApiToGetOrderByOrderId();
  //goi ham load cac option vao select kich co combo tren modal order detail
  loadOptionToSelectComboSize();
  //goi ham goi api de lay danh sach nuoc uong
  callApiToGetDrinkList();
  //goi ham load cac option vao cac select do uong tren modal order detail
  loadOptionToSelectDrink();
  //goi ham load data len modal order detail
  loadDataToOrderDetailModal();
  //hien thi modal order detail
  $('#order-detail-modal').modal('show');
}
//ham xu ly su kien khi an nut loc
function onBtnFilterOrderClick() {
  'use strict';
  //1. thu thap du lieu
  var vFilterData = getFilterData();
  console.log(vFilterData);
  //2. kiem tra du lieu(ko can)
  //3. thuc hien loc du lieu
  var vFilterResult = filterOrder(vFilterData);
  console.log(vFilterResult);
  //4. xu ly hien thi du lieu da loc len bang
  loadDataFilteredToDataTable(vFilterResult);
}
//ham xu ly su kien khi an nut confirm tren modal
function onBtnModalConfirmClick() {
  'use strict';
  //1. thu thap du lieu(ko can)
  //2. kiem tra du lieu(ko can)
  console.log('ID: ' + gId);
  //tao doi tuong luu tru trang thai don hang
  var vObjectRequest = {
    trangThai: 'confirmed', //3: trang thai open, confirmed, cancel tùy tình huống
  };
  //3. goi api de update trang thai don hang
  callApiToUpdateOrder(vObjectRequest);
  //.4 xu ly hien thi front-end
  //goi api de lay du lieu don hang sau khi update
  callApiToGetOrders();
  //goi ham load danh sach don hang len data table
  loadDataToDataTable();
  //an modal order detail
  $('#order-detail-modal').modal('hide');
}
//ham xu ly su kien khi an nut cancel tren modal
function onBtnModalCancelClick() {
  'use strict';
  //1. thu thap du lieu(ko can)
  //2. kiem tra du lieu(ko can)
  console.log('ID: ' + gId);
  //tao doi tuong luu tru trang thai don hang
  var vObjectRequest = {
    trangThai: 'cancel', //3: trang thai open, confirmed, cancel tùy tình huống
  };
  //3. goi api de update trang thai don hang
  callApiToUpdateOrder(vObjectRequest);
  //.4 xu ly hien thi front-end
  //goi api de lay du lieu don hang sau khi update
  callApiToGetOrders();
  //goi ham load danh sach don hang len data table
  loadDataToDataTable();
  //an modal order detail
  $('#order-detail-modal').modal('hide');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load du lieu danh sach don hang len data table
//input: danh sach don hang tra ve tu server
function loadDataToDataTable() {
  'use strict';
  gOrderTable.clear();
  gOrderTable.rows.add(gOrderList);
  gOrderTable.draw();
}
//ham load du lieu danh sach don hang da duoc loc len data table
//input: danh sach don hang da duoc loc
function loadDataFilteredToDataTable(paramFilterData) {
  'use strict';
  gOrderTable.clear();
  gOrderTable.rows.add(paramFilterData);
  gOrderTable.draw();
}
//ham load data len modal order detail
//input: doi tuong order tra ve tu server
function loadDataToOrderDetailModal() {
  'use strict';
  $('#input-order-id').val(gOrder.orderId);
  $('#select-combo-size').val(gOrder.kichCo);
  $('#input-duong-kinh').val(gOrder.duongKinh);
  $('#input-suon-nuong').val(gOrder.suon);
  $('#select-drink').val(gOrder.idLoaiNuocUong);
  $('#input-so-luong-nuoc').val(gOrder.soLuongNuoc);
  $('#input-voucher-id').val(gOrder.idVourcher);
  $('#input-loai-pizza').val(gOrder.loaiPizza);
  $('#input-salad').val(gOrder.salad);
  $('#input-thanh-tien').val(gOrder.thanhTien);
  $('#input-giam-gia').val(gOrder.giamGia);
  $('#input-ho-ten').val(gOrder.hoTen);
  $('#input-email').val(gOrder.email);
  $('#input-so-dien-thoai').val(gOrder.soDienThoai);
  $('#input-dia-chi').val(gOrder.diaChi);
  $('#input-loi-nhan').val(gOrder.loiNhan);
  $('#input-trang-thai').val(gOrder.trangThai);
  $('#input-ngay-tao-don').val(gOrder.ngayTao);
  $('#input-ngay-cap-nhat').val(gOrder.ngayCapNhat);
}
//ham thu thap du lieu can loc
function getFilterData() {
  'use strict';
  var vFilterData = {
    trangThai: '',
    loaiPizza: '',
  };

  vFilterData.trangThai = $('#select-order-status').val();
  vFilterData.loaiPizza = $('#select-pizza-type').val();

  return vFilterData;
}
//ham loc du lieu
//input: doi tuong du lieu can loc
//output: mang cac doi tuong don hang hop le
function filterOrder(paramFilterData) {
  'use strict';
  var vFilterResult = [];
  vFilterResult = gOrderList.filter(function (order) {
    return (
      (order.trangThai.toLowerCase() ===
        paramFilterData.trangThai.toLowerCase() ||
        paramFilterData.trangThai === '') &&
      (order.loaiPizza.toLowerCase() ===
        paramFilterData.loaiPizza.toLowerCase() ||
        paramFilterData.loaiPizza === '')
    );
  });
  return vFilterResult;
}
//ham load cac option vao select trang thai order
function loadOptionToSelectOrderStatus() {
  'use strict';
  var vSelectOrderStatus = $('#select-order-status');
  for (var bI = 0; bI < gORDER_STATUS.length; bI++) {
    var bOption = $(
      `<option value="${gORDER_STATUS[bI]}">${gORDER_STATUS[bI]}</option>"`
    );
    bOption.appendTo(vSelectOrderStatus);
  }
}
//ham load cac option vao select loai pizza
function loadOptionToSelectPizzaType() {
  'use strict';
  var vSelectPizzaType = $('#select-pizza-type');
  for (var bI = 0; bI < gPIZZA_TYPE.length; bI++) {
    var bOption = $(
      `<option value="${gPIZZA_TYPE[bI]}">${gPIZZA_TYPE[bI]}</option>"`
    );
    bOption.appendTo(vSelectPizzaType);
  }
}
//ham load cac option vao select kich co combo tren modal order detail
function loadOptionToSelectComboSize() {
  'use strict';
  console.log(gDrinkList);
  var vSelectComboSize = $('#select-combo-size');
  for (var bI = 0; bI < gCOMBO_SIZE.length; bI++) {
    var bOption = $(
      `<option value="${gCOMBO_SIZE[bI]}">${gCOMBO_SIZE[bI]}</option>"`
    );
    bOption.appendTo(vSelectComboSize);
  }
}
//ham load cac option vao select do uong tren modal order detail
function loadOptionToSelectDrink() {
  'use strict';
  var vSelectDrink = $('#select-drink');
  for (var bI = 0; bI < gDrinkList.length; bI++) {
    var bOption = $(
      `<option value="${gDrinkList[bI].maNuocUong}">${gDrinkList[bI].tenNuocUong}</option>"`
    );
    bOption.appendTo(vSelectDrink);
  }
}
