'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//khai bao bien toan cuc luu ten cua loai nuoc uong duoc chon
var gSelectedDrink = '';
//khai bao bien toan cuc luu tru thong tin don hang
var gOrderInfo = {};
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  //gan ham xu ly su kien cho nut gui khi gui don hang
  $('#btn-send-order').on('click', function (event) {
    event.preventDefault();
    onBtnSendOrderClick();
  });
  //gan ham xu ly su kien khi an nut tao don tren modal
  $('#btn-create-order-modal').on('click', function () {
    onBtnCreateOrderModalClick();
  });
  //gan ham xu ly su kien khi an nut quay lai tren modal
  $('#btn-return-modal').on('click', function () {
    //tat modal di
    $('#order-detail-modal').modal('hide');
  });
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi gui don hang
function onBtnSendOrderClick() {
  'use strict';
  //1. thu thap du lieu
  var vOrder = getOrderData();
  console.log(vOrder);
  //2. kiem tra du lieu
  var vIsValidate = validateData(vOrder);
  console.log('Ket qua kiem tra: ' + vIsValidate);
  if (vIsValidate) {
    //3. xu ly hien thi front-end
    //goi ham thu thap du lieu vao bien toan cuc gOrderInfo de tao don hang
    getOrderInfo(vOrder);
    console.log('order info:', gOrderInfo);
    //xoa trang cac o input cua modal
    resetInputModal();
    //gan du lieu tu form vao input tren modal
    loadOrderDataToModal(vOrder);
    // hien thi modal thong tin don hang
    $('#order-detail-modal').modal('show');
  }
}
//ham xu ly su kien khi an nut tao don tren modal
function onBtnCreateOrderModalClick() {
  'use strict';
  //goi ham goi api de tao don hang
  callApiToCreateOrder(gOrderInfo);
  //an modal thong tin don hon hang
  $('#order-detail-modal').modal('hide');
  //load du lieu order id tra ve tu server sau khi tao don len modal
  $('#input-order-id-modal').val(gOrderId);
  //gan su kien cho modal, reload lai page neu nguoi dung dong modal order succesful
  $('#order-succesful-modal').on('hidden.bs.modal', function () {
    location.reload();
  });
  //hien thi modal dat hang thanh cong
  $('#order-succesful-modal').modal('show');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham thu thap du lieu don hang
function getOrderData() {
  'use strict';
  //lay cac gia tri tu input
  var vInputNameValue = $('#input-name').val().trim();
  var vInputEmailValue = $('#input-email').val().trim();
  var vInputPhoneNumberValue = $('#input-phone-number').val().trim();
  var vInputAddressValue = $('#input-address').val().trim();
  var vInputVoucherValue = $('#input-voucher').val().trim();
  var vInputMessageValue = $('#input-message').val().trim();
  var vSelectedDrinkValue = $('#select-drink option:selected').text();
  gSelectedDrink = $('#select-drink').val();
  //goi ham goi api de lay ma giam gia(neu co)
  callApiToGetVoucher(vInputVoucherValue);
  //neu ma giam gia khong ton tai, alert cho nguoi dung biet
  if (gDiscountPercent === 0) {
    alert('Mã giảm giá không tồn tại');
  }
  //khai bao doi tuong luu tru thong tin don hang
  var vOrder = {
    menuDuocChon: gSelectedMenuCombo,
    loaiPizza: gSelectedPizzaType,
    loaiNuocUong: vSelectedDrinkValue,
    hoTen: vInputNameValue,
    email: vInputEmailValue,
    soDienThoai: vInputPhoneNumberValue,
    diaChi: vInputAddressValue,
    voucher: vInputVoucherValue,
    loiNhan: vInputMessageValue,
    //phuong thuc tinh gia tien (co bao gom voucher giam gia)
    thanhTien: function () {
      var vTotalPrice = 0;
      if (gDiscountPercent !== 0) {
        vTotalPrice = this.menuDuocChon.gia * (1 - gDiscountPercent / 100);
      } else {
        vTotalPrice = this.menuDuocChon.gia;
      }
      return vTotalPrice;
    },
  };
  return vOrder;
}
//ham kiem tra du lieu don hang
//input: doi tuong thong tin don hang
//output: true/false neu hop le/khong hop le
function validateData(paramOrderObj) {
  'use strict';
  var vResult = true;
  if (paramOrderObj.menuDuocChon === '') {
    vResult = false;
    alert('Vui lòng chọn kích cỡ combo bạn muốn dùng');
  }
  if (paramOrderObj.loaiPizza === '') {
    vResult = false;
    alert('Vui lòng chọn loại pizza bạn muốn dùng');
  }
  if (paramOrderObj.loaiNuocUong === 'NOT-SELECTED-DRINK') {
    vResult = false;
    alert('Vui lòng chọn loại nước uống bạn muốn dùng');
  }
  if (paramOrderObj.hoTen === '') {
    vResult = false;
    alert('Vui lòng nhập họ và tên của bạn');
  }
  if (paramOrderObj.diaChi === '') {
    vResult = false;
    alert('Vui lòng nhập địa chỉ của bạn');
  }
  if (validatePhoneNumber(paramOrderObj.soDienThoai) === false) {
    vResult = false;
  }
  if (validateEmail(paramOrderObj.email) === false) {
    vResult = false;
  }

  return vResult;
}
//ham kiem tra email
//input: email can kiem tra
//output: true/false neu hop le/khong hop le
function validateEmail(paramEmail) {
  'use strict';
  var vResultEmail = true;
  var validRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (paramEmail === '' || paramEmail.length < 3) {
    vResultEmail = false;
    alert('Vui lòng nhập email của bạn');
  } else {
    if (!paramEmail.match(validRegex)) {
      vResultEmail = false;
      alert('Email không hợp lệ, ví dụ email hợp lệ: abc@xyz.com');
    }
  }
  return vResultEmail;
}
//ham kiem tra so dien thoai
//input: so dien thoai can kiem tra
//output: true/false neu hop le/khong hop le
function validatePhoneNumber(paramPhoneNumber) {
  'use strict';
  var vResultPhoneNumber = true;
  var vPhoneNumber = parseInt(paramPhoneNumber);
  if (vPhoneNumber === '') {
    vResultPhoneNumber = false;
    alert('Vui lòng nhập số điện thoại');
  } else {
    if (isNaN(vPhoneNumber)) {
      vResultPhoneNumber = false;
      alert('Số điện thoại phải là dãy số');
    }
  }
  return vResultPhoneNumber;
}
//ham xoa trang cac o input cua modal
function resetInputModal() {
  'use strict';
  $('#input-name-modal').val('');
  $('#input-email-modal').val('');
  $('#input-phone-number-modal').val('');
  $('#input-address-modal').val('');
  $('#input-message-modal').val('');
  $('#txtarea-order-info-modal').val('');
}
//ham gan du lieu tu form vao input tren modal
function loadOrderDataToModal(paramOrderObj) {
  'use strict';
  var vInputNameModalElement = $('#input-name-modal');
  var vInputEmailModalElement = $('#input-email-modal');
  var vInputPhoneNumberModalElement = $('#input-phone-number-modal');
  var vInputAddressElement = $('#input-address-modal');
  var vInputMessageModalElement = $('#input-message-modal');
  var vTxtareaOrderInfoModalElement = $('#txtarea-order-info-modal');
  var vOrderDetail = `Khách hàng: ${paramOrderObj.hoTen}, ${
    paramOrderObj.soDienThoai
  }, ${paramOrderObj.diaChi}.
Combo: size ${paramOrderObj.menuDuocChon.menuName}, đường kính ${
    paramOrderObj.menuDuocChon.duongKinh
  } cm, ${paramOrderObj.menuDuocChon.suonNuong} sườn nướng, ${
    paramOrderObj.menuDuocChon.salad
  }g salad, ${paramOrderObj.menuDuocChon.nuocNgot} ${
    paramOrderObj.loaiNuocUong
  }.
Loại pizza: ${paramOrderObj.loaiPizza}, giá: ${
    paramOrderObj.menuDuocChon.gia
  } VNĐ, mã giảm giá: ${paramOrderObj.voucher}.
Phải thanh toán: ${paramOrderObj.thanhTien()} VNĐ (giảm giá ${gDiscountPercent}%).`;

  vInputNameModalElement.val(paramOrderObj.hoTen);
  vInputEmailModalElement.val(paramOrderObj.email);
  vInputPhoneNumberModalElement.val(paramOrderObj.soDienThoai);
  vInputAddressElement.val(paramOrderObj.diaChi);
  vInputMessageModalElement.val(paramOrderObj.loiNhan);
  vTxtareaOrderInfoModalElement.val(vOrderDetail);
}
//ham thu thap du lieu vao bien toan cuc gOrderInfo de tao don hang
//input: doi tuong chua thong tin don hang
function getOrderInfo(paramOrderObj) {
  'use strict';
  gOrderInfo.kichCo = gSelectedMenuCombo.menuName;
  gOrderInfo.duongKinh = gSelectedMenuCombo.duongKinh;
  gOrderInfo.suon = gSelectedMenuCombo.suonNuong;
  gOrderInfo.salad = gSelectedMenuCombo.salad;
  gOrderInfo.loaiPizza = gSelectedPizzaType;
  gOrderInfo.idVourcher = paramOrderObj.voucher;
  gOrderInfo.idLoaiNuocUong = gSelectedDrink;
  gOrderInfo.soLuongNuoc = gSelectedMenuCombo.nuocNgot;
  gOrderInfo.hoTen = paramOrderObj.hoTen;
  gOrderInfo.thanhTien = paramOrderObj.thanhTien();
  gOrderInfo.email = paramOrderObj.email;
  gOrderInfo.soDienThoai = paramOrderObj.soDienThoai;
  gOrderInfo.diaChi = paramOrderObj.diaChi;
  gOrderInfo.loiNhan = paramOrderObj.loiNhan;
}
