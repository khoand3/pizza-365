'use strict';
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
//bien toan cuc luu tru thong tin menu combo duoc chon
var gSelectedMenuCombo = '';
//bien toan cuc luu tru thong tin loai pizza duoc chon
var gSelectedPizzaType = '';
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
  //goi ham xu ly su kien khi load trang
  onPageLoading();
  //gan ham xu ly su kien khi an nut chon menu size S
  $('#btn-combo-size-s').on('click', onBtnComboSizeSClick);
  //gan ham xu ly su kien khi an nut chon menu size M
  $('#btn-combo-size-m').on('click', onBtnComboSizeMClick);
  //gan ham xu ly su kien khi an nut chon menu size L
  $('#btn-combo-size-l').on('click', onBtnComboSizeLClick);
  //gan ham xu ly su kien khi an nut chon pizza seafood
  $('#btn-pizza-seafood').on('click', onBtnPizzaSeafoodClick);
  //gan ham xu ly su kien khi an nut chon pizza hawaii
  $('#btn-pizza-hawaii').on('click', onBtnPizzaHawaiiClick);
  //gan ham xu ly su kien khi an nut chon pizza bacon
  $('#btn-pizza-bacon').on('click', onBtnPizzaBaconClick);
});
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
//ham xu ly su kien khi load trang
function onPageLoading() {
  'use strict';
  //goi ham goi api de lay daily campaign message
  callApiToGetDailyCampaignMessage();
  //goi ham goi api de lay combo menu
  callApiToGetComboMenu();
  //goi ham load combo menu len giao dien
  loadDataToComboMenuPanel(gComboMenu);
  //goi ham goi api de lay danh sach nuoc uong
  callApiToGetDrinkList();
  //goi ham load du lieu nuoc uong len select
  loadDataToSelectDrink();
}
//ham xu ly su kien khi an nut chon menu size S
function onBtnComboSizeSClick() {
  'use strict';
  //khai bao bien luu doi tuong menu duoc chon
  var vSelectedMenu = getMenuCombo('S', '20', '2', '200', '2', '150000');
  //goi phuong thuc in thong tin menu duoc chon
  vSelectedMenu.displayInConsole();
  //doi mau button
  changeMenuButtonColor('S');
  //luu doi tuong menu duoc chon vao bien toan cuc
  gSelectedMenuCombo = vSelectedMenu;
}
//ham xu ly su kien khi an nut chon menu size M
function onBtnComboSizeMClick() {
  'use strict';
  //khai bao bien luu doi tuong menu duoc chon
  var vSelectedMenu = getMenuCombo('M', '25', '4', '300', '3', '200000');
  //goi phuong thuc in thong tin menu duoc chon
  vSelectedMenu.displayInConsole();
  //doi mau button
  changeMenuButtonColor('M');
  //luu doi tuong menu duoc chon vao bien toan cuc
  gSelectedMenuCombo = vSelectedMenu;
}
//ham xu ly su kien khi an nut chon menu size L
function onBtnComboSizeLClick() {
  'use strict';
  //khai bao bien luu doi tuong menu duoc chon
  var vSelectedMenu = getMenuCombo('L', '30', '8', '500', '4', '250000');
  //goi phuong thuc in thong tin menu duoc chon
  vSelectedMenu.displayInConsole();
  //doi mau button
  changeMenuButtonColor('L');
  //luu doi tuong menu duoc chon vao bien toan cuc
  gSelectedMenuCombo = vSelectedMenu;
}
//ham xu ly khi an nut chon cua pizza seafood
function onBtnPizzaSeafoodClick() {
  'use strict';
  //gan gia tri bien toan cuc gSelectedPizzaType bang ten pizza duoc chon
  gSelectedPizzaType = 'Seafood';
  // hien thi ten Pizza duoc chon
  console.log('%cPIZZA SELECTED - ' + gSelectedPizzaType, 'color:green');
  // đổi màu button
  changeSelectPizzaTypeButtonColor('Seafood');
}
//ham xu ly khi an nut chon cua pizza hawaii
function onBtnPizzaHawaiiClick() {
  'use strict';
  //gan gia tri bien toan cuc gSelectedPizzaType bang ten pizza duoc chon
  gSelectedPizzaType = 'Hawaii';
  // hien thi ten Pizza duoc chon
  console.log('%cPIZZA SELECTED - ' + gSelectedPizzaType, 'color:green');
  // đổi màu button
  changeSelectPizzaTypeButtonColor('Hawaii');
}
//ham xu ly khi an nut chon cua pizza bacon
function onBtnPizzaBaconClick() {
  'use strict';
  //gan gia tri bien toan cuc gSelectedPizzaType bang ten pizza duoc chon
  gSelectedPizzaType = 'Bacon';
  // hien thi ten Pizza duoc chon
  console.log('%cPIZZA SELECTED - ' + gSelectedPizzaType, 'color:green');
  // đổi màu button
  changeSelectPizzaTypeButtonColor('Bacon');
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
//ham load combo menu len giao dien
//input: mang chua thong tin cac combo menu
function loadDataToComboMenuPanel(paramComboMenu) {
  'use strict';
  var vComboMenuPanel = $('#combo-menu-price-panel');
  for (var bI = 0; bI < paramComboMenu.length; bI++) {
    var vComboMenuHeader = getComboMenuHeader(paramComboMenu[bI].size);
    var vColorHeader = '';
    if (paramComboMenu[bI].size === 'M') {
      vColorHeader = 'bg-warning';
    } else {
      vColorHeader = 'background-primary-color';
    }
    var vComboMenuCard = $(`
      <div class="card text-center">
        <div class="card-header ${vColorHeader}">
          <h3>${vComboMenuHeader}</h3>
        </div>
      <div class="card-body">
        <p class="card-text">Đường kính: <strong>${
          paramComboMenu[bI].duongKinh
        }cm</strong></p>
        <hr />
        <p class="card-text">Sườn nướng: <strong>${
          paramComboMenu[bI].suonNuong
        }</strong></p>
        <hr />
        <p class="card-text">Salad: <strong>${
          paramComboMenu[bI].salad
        }g</strong></p>
        <hr />
        <p class="card-text">Nước ngọt: <strong>${
          paramComboMenu[bI].nuocNgot
        }</strong></p>
        <hr />
        <p class="card-text mb-0">
          <strong class="price-text">${paramComboMenu[bI].gia.substring(
            0,
            3
          )}.${paramComboMenu[bI].gia.substring(3, 6)}</strong>
        </p>
        <span>VNĐ</span>
      </div>
        <div class="card-footer">
          <button
            class="btn w-100 background-primary-color font-weight-500"
            id="btn-combo-size-${paramComboMenu[bI].size.toLowerCase()}"
          >
            Chọn
        </button>
      </div>
    </div>`);
    vComboMenuCard.appendTo(vComboMenuPanel);
  }
}
//ham tra ve text cua header combo menu tuong ung voi tung size
//input: mang chua thong tin cac combo menu
function getComboMenuHeader(paramComboMenuSize) {
  'use strict';
  var vComboMenuHeader = '';
  switch (paramComboMenuSize) {
    case 'S':
      vComboMenuHeader = 'S (small)';
      break;
    case 'M':
      vComboMenuHeader = 'M (medium)';
      break;
    case 'L':
      vComboMenuHeader = 'L (large)';
      break;
  }
  return vComboMenuHeader;
}
//ham lay thong tin menu combo duoc chon
//input: cac thong tin cua menu
function getMenuCombo(
  paramMenuDuocChon,
  paramDuongKinh,
  paramSuonNuong,
  paramSalad,
  paramNuocNgot,
  paramGia
) {
  'use strict';
  var vMenuCombo = {
    menuName: paramMenuDuocChon,
    duongKinh: paramDuongKinh,
    suonNuong: paramSuonNuong,
    salad: paramSalad,
    nuocNgot: paramNuocNgot,
    gia: paramGia,
    displayInConsole: function () {
      console.log('%cMENU SELECTED - ' + this.menuName, 'color:green');
      console.log('Đường kính: ' + this.duongKinh + ' cm');
      console.log('Sườn nướng: ' + this.suonNuong);
      console.log('Salad: ' + this.salad + 'g');
      console.log('Nước ngọt: ' + this.nuocNgot);
      console.log('Giá: ' + this.gia + ' VNĐ');
    },
  };
  return vMenuCombo;
}
//ham doi mau nut "chon" khi chon menu combo
//input: ten menu duoc chon
function changeMenuButtonColor(paramMenuName) {
  'use strict';
  var vBtnMenuComboSizeSElement = $('#btn-combo-size-s');
  var vBtnMenuComboSizeMElement = $('#btn-combo-size-m');
  var vBtnMenuComboSizeLElement = $('#btn-combo-size-l');
  if (paramMenuName === 'S') {
    vBtnMenuComboSizeSElement
      .removeClass('background-primary-color')
      .addClass('btn-warning');
    vBtnMenuComboSizeMElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnMenuComboSizeLElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
  } else if (paramMenuName === 'M') {
    vBtnMenuComboSizeSElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnMenuComboSizeMElement
      .removeClass('background-primary-color')
      .addClass('btn-warning');
    vBtnMenuComboSizeLElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
  } else if (paramMenuName === 'L') {
    vBtnMenuComboSizeSElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnMenuComboSizeMElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnMenuComboSizeLElement
      .removeClass('background-primary-color')
      .addClass('btn-warning');
  }
}
//ham doi mau nut "chon" khi chon loai pizza
//input: loai pizza duoc chon
function changeSelectPizzaTypeButtonColor(paramPizzaType) {
  'use strict';
  var vBtnPizzeSeafoodElement = $('#btn-pizza-seafood');
  var vBtnPizzaHawaiiElement = $('#btn-pizza-hawaii');
  var vBtnPizzaBaconElement = $('#btn-pizza-bacon');
  if (paramPizzaType === 'Seafood') {
    vBtnPizzeSeafoodElement
      .removeClass('background-primary-color')
      .addClass('btn-warning');
    vBtnPizzaHawaiiElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnPizzaBaconElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
  } else if (paramPizzaType === 'Hawaii') {
    vBtnPizzeSeafoodElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnPizzaHawaiiElement
      .removeClass('background-primary-color')
      .addClass('btn-warning');
    vBtnPizzaBaconElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
  } else if (paramPizzaType === 'Bacon') {
    vBtnPizzeSeafoodElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnPizzaHawaiiElement
      .removeClass('btn-warning')
      .addClass('background-primary-color');
    vBtnPizzaBaconElement
      .removeClass('background-primary-color')
      .addClass('btn-warning');
  }
}
//ham load du lieu nuoc uong len select
function loadDataToSelectDrink() {
  'use strict';
  var vSelectDrinkElement = $('#select-drink');
  //duyet qua mang nuoc uong tra ve tu server
  for (var bI = 0; bI < gDrinkList.length; bI++) {
    //tao phan tu option
    var bOption = $(
      `<option value="${gDrinkList[bI].maNuocUong}">${gDrinkList[bI].tenNuocUong}</option>`
    );
    //them option vao select
    bOption.appendTo(vSelectDrinkElement);
  }
}
