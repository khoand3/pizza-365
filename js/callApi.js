'use strict';
const gBASE_URL = 'http://42.115.221.44:8080/devcamp-pizza365/orders';
//khai bao bien toan cuc luu tru danh sach nuoc uong tra ve server
var gDrinkList;
//khai bao bien toan cuc luu tru phan tram giam gia tra ve server
var gDiscountPercent = 0;
//khai bao bien toan cuc luu tru order id sau khi tao don
var gOrderId;
//khai bao bien toan cuc chua du lieu order khi server tra ve
var gOrderList;
//khai bao bien toan cuc chua du lieu order theo order id khi server tra ve
var gOrder;
//khai bao bien toan cuc chua du lieu combo menu khi server tra ve
var gComboMenu;
//ham goi api de lay du lieu tat ca don hang
function callApiToGetOrders() {
  'use strict';
  $.ajax({
    url: gBASE_URL,
    type: 'GET',
    dataType: 'JSON',
    async: false,
    success: function (responseData) {
      //gan gia tri tra ve vao bien toan cuc
      gOrderList = responseData;
    },
    error: function () {
      console.log(error);
    },
  });
}
//ham goi api de lay du lieu don hang theo order id
function callApiToGetOrderByOrderId() {
  'use strict';
  $.ajax({
    url: gBASE_URL + '/' + gOrderId,
    type: 'GET',
    dataType: 'JSON',
    async: false,
    success: function (responseData) {
      //gan gia tri tra ve vao bien toan cuc
      gOrder = responseData;
    },
    error: function () {
      console.log(error);
    },
  });
}
//ham goi api de tao don hang
//input: doi tuong thong tin don hang
function callApiToCreateOrder(paramOrderInfo) {
  'use strict';
  $.ajax({
    url: gBASE_URL,
    type: 'POST',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(paramOrderInfo),
    async: false,
    success: function (responseData) {
      console.log(responseData);
      //thong bao tao don hang thanh cong
      alert('Tạo đơn hàng thành công!');
      gOrderId = responseData.orderId;
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de update don hang
function callApiToUpdateOrder(paramObjectRequest) {
  'use strict';
  $.ajax({
    url: gBASE_URL + '/' + gId,
    type: 'PUT',
    contentType: 'application/json;charset=UTF-8',
    data: JSON.stringify(paramObjectRequest),
    async: false,
    success: function (responseData) {
      console.log(responseData);
      //thong bao tao don hang thanh cong
      alert('Cập nhật đơn hàng thành công!');
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de lay danh sach nuoc uong
function callApiToGetDrinkList() {
  'use strict';
  // const vBASE_URL = 'http://42.115.221.44:8080/devcamp-pizza365/drinks';
  const vBASE_URL = 'http://localhost:8080/drinks/';
  $.ajax({
    url: vBASE_URL,
    type: 'GET',
    dataType: 'JSON',
    async: false,
    success: function (responseData) {
      //gan gia tri tra ve vao bien toan cuc
      gDrinkList = responseData;
    },
    error: function () {
      console.log(error);
    },
  });
}
//ham goi api de kiem tra voucher
//input: voucher id can kiem tra
function callApiToGetVoucher(paramVoucherId) {
  'use strict';
  const vBASE_URL =
    'http://42.115.221.44:8080/devcamp-voucher-api/voucher_detail/';
  $.ajax({
    url: vBASE_URL + paramVoucherId,
    type: 'GET',
    dataType: 'JSON',
    async: false,
    success: function (responseData) {
      //luu phan tram giam gia vao bien toan cuc
      gDiscountPercent = responseData.phanTramGiamGia;
    },
    error: function (error) {
      console.log(error);
    },
  });
}
//ham goi api de lay daily campaign message
function callApiToGetDailyCampaignMessage() {
  'use strict';
  const vBASE_URL = 'http://localhost:8080/campaigns/daily-campaign';
  $.ajax({
    url: vBASE_URL,
    type: 'GET',
    dataType: 'Text',
    success: function (response) {
      console.log(response);
      //hien thi ket qua tra ve len web
      $('#daily-campaign-message').html(response);
    },
    error: function (error) {
      console.log(error.responseText);
    },
  });
  // var vXmlHttp = new XMLHttpRequest();
  // vXmlHttp.open('GET', vBASE_URL, true);
  // vXmlHttp.send();
  // vXmlHttp.onreadystatechange = function () {
  //   if (this.readyState == 4 && this.status == 200) {
  //     var message = vXmlHttp.responseText;
  //     console.log(message);
  //     document.getElementById('daily-campaign-message').innerHTML = message;
  //   }
  // };
}
//ham goi api de lay combo menu
function callApiToGetComboMenu() {
  'use strict';
  const vBASE_URL = 'http://localhost:8080/combomenu/';
  $.ajax({
    url: vBASE_URL,
    type: 'GET',
    dataType: 'JSON',
    async: false,
    success: function (responseData) {
      gComboMenu = responseData;
      console.log(gComboMenu);
    },
    error: function (error) {
      console.log(error.responseText);
    },
  });
}
